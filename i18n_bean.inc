<?php
/**
 * Blocks textgroup handler
 */

/**
 * Bean object
 */
class i18n_bean_object extends i18n_string_object_wrapper {

  /**
   * Load a bean object.
   *
   * @param $object
   *  An array with delta.
   */
  function load_object($object) {
    $this->object = call_user_func_array($this->get_info('load callback'), $object);
    return $this->get_object();
  }

  /**
   * Get base keys for translating this object
   */
  public function get_string_context() {
    return array($this->object->type, $this->object->delta);
  }

  /**
   * Get object strings for translation
   */
  protected function build_properties() {
    $properties = parent::build_properties();

    // Unset unused fields
    $field_info_instances = field_info_instances('bean', $this->object->type);
    foreach (array_keys($properties['bean'][$this->object->type][$this->object->delta]) as $property) {
      list($field_name, $lang, $delta, $column) = explode('.', $property);
      if ($field_name !== 'title' && !isset($field_info_instances[$field_name])) {
        unset($properties['bean'][$this->object->type][$this->object->delta][$property]);
      } elseif ($field_name != 'title')  {
        // Set a better label
        $label = $properties['bean'][$this->object->type][$this->object->delta][$property]['title'];
        $properties['bean'][$this->object->type][$this->object->delta][$property]['title'] = $field_info_instances[$field_name]['label'] . " $column #$delta <div class='description'>$label</div>";
      }
    }

    return $properties;
  }

  /**
   * Translation mode for object
   */
  public function get_translate_mode() {
    return !empty($this->object->i18n_mode) ? I18N_MODE_LOCALIZE : I18N_MODE_NONE;
  }
}
