<?php
/**
 * @file
 * Internationalization (i18n) hooks
 */


/**
 * Implements hook_i18n_object_info().
 */
function i18n_bean_i18n_object_info() {
  $info['bean'] = array(
    'title' => t('Bean'),
    'class' => 'i18n_bean_object',
    'load callback' => 'bean_load_delta',
    'key' => 'delta',
    'placeholders' => array(
      '%delta' => 'delta',
    ),
    'edit path' => 'block/%delta/edit',
    'string translation' => array(
      'textgroup' => 'bean',
      'properties' => array(
        'title' => array(
          'title' => t('Title'),
          'empty' => '<none>',
        ),
      ),
      'translate path' => 'block/%delta/translate/%i18n_language',
    )
  );

  // Add fields for all bean bundles. Unset irrelevant fields in i18n_bean_object::get_properties.
  foreach (bean_get_types() as $name => $bean) {
    $field_info_instance = field_info_instances('bean', $name);
    foreach ($field_info_instance as $field_name => $field) {
      $field_info = field_info_field($field_name);
      $info['bean']['string translation']['properties'] += _i18n_bean_properties($field_info);
    }
  }

  return $info;
}


/**
 * Implements hook_i18n_string_info().
 */
function i18n_bean_i18n_string_info() {
  $groups['bean'] = array(
    'title' => t('Bean'),
    'description' => t('Localize bean strings.'),
    'format' => TRUE, // This group has strings with format (block body)
    'list' => TRUE, // This group can list all strings
  );
  return $groups;
}


/**
 * Helper functions
 * @param $field_info
 */
function _i18n_bean_properties($field_info) {
  $properties = array();

  if (!in_array($field_info['module'], array('link', 'text'))) {
    return $properties;
  }

  $count = ($field_info['cardinality'] > -1 ? $field_info['cardinality'] : 5);
  for ($i = 0; $i < $count; $i++) {
    foreach (array_keys($field_info['columns']) as $key) {
      // don't need to translate format and attributes
      if (!in_array($key, array('format', 'attributes'))) {
        $field_id = "{$field_info['field_name']}.und.$i.$key";
        $properties[$field_id] = array(
          'title' => $field_id,
          'field' => $field_id
        );
      }
      // get the format of the text field
      if (isset($field_info['columns']['format'])) {
        $properties[$field_id]['format'] = "{$field_info['field_name']}.und.$i.format";
      }
    }
  }
  return $properties;
}
